## <center>.:: Registro de viajes nuevos ::.
---

## <center>INTRO
 Este programa va a estar echo para gestionar una agencia de viajes, en el mismo se va a poder agregar un pasajero con su destino, lugar de subida, horario y vehiculo en el que va a viajar. Estos van a estar agregados en una lista, entonces si por ejemplo un destino no está agregado previamente, no se va a poder guardar un nuevo viaje que contenga este destino no guardado. 
 
 <br> Se va a realizar la programación del mismo con *Java* y persistir en una base de datos utlizando *MySql*.
 
 
- ### Primeros 3 commits<br> 
	- *Progreso:*<br>
	Se crearon clases y métodos
	básicos para comenzar el 
	funcionamiento del programa. 

	- *complicaciones:* <br>
	     ninguna por el momento.

- ### 4° Commit<br> 
  - *progreso:*<br>
  Se siguieron agregando métodos,
	hice cambios en nombres de clases
	y el papel que cumple cada uno.
	Se realizo una primera impresion
	de los resultados que va a dar
	el programa.
	
	

  - *complicaciones:*<br>
		Se produjo un error de ejecucion,tratando de buscar el inconveniente se utilizó el debugger que viene con el IDE Eclipse y se llegó a corregir el problema.
		

		