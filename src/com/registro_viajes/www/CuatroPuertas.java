package com.registro_viajes.www;

public class CuatroPuertas extends Vehiculo{

	private int maximoDeMaletas;
	
	public CuatroPuertas(String matricula, int velocidad, int cantidadMaxima, int maximoDeMaletas) {
		
		super( matricula,  velocidad,  cantidadMaxima);
		
		this.maximoDeMaletas=maximoDeMaletas;
	}
	
	public int getMaximoDeMaletas() {
		return maximoDeMaletas;
	}
}
