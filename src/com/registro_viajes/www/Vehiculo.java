package com.registro_viajes.www;

public class Vehiculo {
	
	private  String matricula;
	private int velocidad;
	private int cantidadMaxima;
	public String getMatricula() {
		return matricula;
	}
	
	//constructor
	public Vehiculo(String matricula, int velocidad, int cantidadMaxima) {
		this.matricula = matricula;
		this.velocidad = velocidad;
		this.cantidadMaxima = cantidadMaxima;
	}
	
	public int getVelocidad() {
		return velocidad;
	}
	
	public int getCantidadMaxima() {
		return cantidadMaxima;
	}
	

	
	

}
