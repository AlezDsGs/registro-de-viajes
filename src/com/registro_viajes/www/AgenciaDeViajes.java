package com.registro_viajes.www;

import java.util.ArrayList;
import java.util.List;

public class AgenciaDeViajes {
	
	private String nombre;
	
	private List <Vehiculo> vehiculos;
	private List <Destino> destinos;
	private List <LugarDeSubida> lugaresSubida;
	private List <Horarios> horarios;
	private List <NuevoViaje> viajesNuevos;
	
	
	//CONSTRUCTOR
	
	 public AgenciaDeViajes(String nombre) {
		this.nombre=nombre;
		vehiculos=new ArrayList<Vehiculo>();
		destinos=new ArrayList<Destino>();
		lugaresSubida=new ArrayList<LugarDeSubida>();
		horarios=new ArrayList<Horarios>();
		viajesNuevos=new ArrayList<NuevoViaje>();
	}
	 
	 
	 public String getNombre() {
		 return nombre;
	 }

	 //agregar nuevos vehiculos
	 
	public void nuevoVehiculo(Vehiculo vehiculo) {
		
		vehiculos.add(vehiculo);
		
	}
	
	public void nuevoDestino(Destino destino) {
		
		destinos.add(destino);
	}
	
	public void nuevoSubida(LugarDeSubida subida) {
	
	lugaresSubida.add(subida);
	
	}
	
	public void nuevoHorario(Horarios horario) {
	
		horarios.add(horario);
	
	}


	//AGREGAR NUEVO VIAJE
	public  void NuevoViajes(String nombre,int dni,int telefono,String matriculaVehiculo,String destino,String subida,String horario) {
		
		Pasajero pasajero=new Pasajero(nombre,dni,telefono);
		Vehiculo ve=comprobarVehiculo(matriculaVehiculo);
		Destino de=comprobarDestino( destino);
		LugarDeSubida luSub=comprobarSubida(subida);
		Horarios hor=comprobarHorario(horario);
		
	
	//MEJOR CAMBIARLO POR UN TRY CATCH
		if (pasajero!=null && ve!=null && de!=null &&luSub !=null && hor != null) {
			
			NuevoViaje viajerox = new NuevoViaje(pasajero,ve,de,luSub,hor);
			viajesNuevos.add(viajerox);
		}else {
			System.out.println("error alguno de los items agregados no existe");
		}
		
	}
	
	//imprimir viajes nuevos
	
	public void imprimirViajes() {
		
		for(NuevoViaje n:this.viajesNuevos)
			System.out.println(n.toString());
		
	}
	
	//COMPROBADORES DE NUEVOS VIAJES
	
	private Vehiculo comprobarVehiculo(String matriculaVehiculo) {
		
		for (Vehiculo v:this.vehiculos)
			if(v.getMatricula().equals(matriculaVehiculo))
				return v;
		
		return null;

		
	}
	
	private Destino comprobarDestino(String destino) {
		
		for (Destino v:this.destinos)
			if(v.getNombreDestino().equals(destino))
				return v;
		
		return null;
		
	}
	
	private LugarDeSubida comprobarSubida(String subida) {
		
		for (LugarDeSubida v:this.lugaresSubida)
			if(v.getLugarSubida().equals(subida))
				return v;
		
		return null;
		
	}
	
	public Horarios comprobarHorario(String horario) {
		
		for (Horarios v:this.horarios)
			if(v.getHorario().equals(horario))
				return v;
		
		return null;
		
	}
	
	
	
	
}
