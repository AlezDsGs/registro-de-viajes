package com.registro_viajes.www;

public class NuevoViaje {

	
	private Pasajero pasajero;
	private Vehiculo vehiculo;
	private Destino destino;
	private LugarDeSubida subida;
	private Horarios horario;
	
	
//constructor
	public NuevoViaje(Pasajero pasajero,Vehiculo vehiculo, Destino destino,
			LugarDeSubida subida, Horarios horario) {
		
		this.pasajero = pasajero;
		this.vehiculo = vehiculo;
		this.destino = destino;
		this.subida = subida;
		this.horario = horario;
		
	}
	

	//ToString
	public String toString() {
		return "NuevoViaje \n[pasajero=" +
				pasajero.getDni() + ", Vehiculo=" + vehiculo.getMatricula() +
				"\n, destino=" + destino.getNombreDestino() + 
				"\n subida="+ subida.getLugarSubida() + ", horario=" + horario.getHorario();
	}
	
	
	
	

		
	

}
