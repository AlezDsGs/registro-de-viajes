package com.registro_viajes.www;

public class ProgramaPrincipal {

	public static void main(String[] args) {
		
		AgenciaDeViajes travelRock=new AgenciaDeViajes("TRAVEL ROCK");
		
		travelRock.nuevoVehiculo(new Furgoneta("qwerty",90,20,100));
		travelRock.nuevoVehiculo(new CuatroPuertas("asdfg", 150, 4, 2));
		travelRock.nuevoVehiculo(new Vehiculo("zxcvb",300,200));
		
		travelRock.nuevoSubida(new LugarDeSubida("mar-Del-Plata"));
		travelRock.nuevoSubida(new LugarDeSubida("Gran Buenos Aires"));
		travelRock.nuevoSubida(new LugarDeSubida("Capital Federal"));
		
		travelRock.nuevoHorario(new Horarios("manana"));
		travelRock.nuevoHorario(new Horarios("tarde"));
		travelRock.nuevoHorario(new Horarios("noche"));
		
		travelRock.nuevoDestino(new Destino("uruguay"));
		travelRock.nuevoDestino(new Destino("chile"));
		travelRock.nuevoDestino(new Destino("mexico"));
		travelRock.nuevoDestino(new Destino("brasil"));
		
		travelRock.NuevoViajes("pepito", 35034766, 1125340867, "asdfg","uruguay","Capital Federal","tarde");
		
		travelRock.NuevoViajes("juancito", 35034766, 1125340867, "uiopy","uruguay","Capital Federal","tarde");
		
		travelRock.imprimirViajes();
		
	}

}
