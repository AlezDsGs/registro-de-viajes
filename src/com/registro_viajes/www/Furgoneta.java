package com.registro_viajes.www;

public class Furgoneta extends Vehiculo{
	
	private int pesoMaximo;
	
	public Furgoneta(String matricula, int velocidad, int cantidadMaxima,int pesoMaximo) {
		
		super( matricula, velocidad, cantidadMaxima);
		
		this.pesoMaximo=pesoMaximo;
			
	}
	
	public int getpesoMaximo(){
		return pesoMaximo;
	}
	
	
	
	

}
