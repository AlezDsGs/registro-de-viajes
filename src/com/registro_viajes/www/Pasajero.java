package com.registro_viajes.www;

public class Pasajero {
	
	private  String nombre;
	private  int dni;
	private  int telefono;
	
	//constructor
	public Pasajero(String nombre, int dni, int telefono) {
		this.nombre = nombre;
		this.dni = dni;
		this.telefono = telefono;
	}
	public String getNombre() {
		return nombre;
	}
	
	public int getDni() {
		return dni;
	}
	
	public int getTelefono() {
		return telefono;
	}
	
	
	

}
